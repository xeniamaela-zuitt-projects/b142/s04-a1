package com.abano.b142.s02.s02app.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name="users")
public class User {

    //Properties (columns)
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id; //primary key
    @Column
    private String username;
    @Column
    private String password;
    //new added code
    @OneToMany(mappedBy = "user")
    @JsonIgnore //to prevent infinite recursion
    private Set<Post> posts;

    //Constructors
    public User(){}
    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }

    //Getters & Setters
    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Set<Post> getPosts() {
        return posts;
    }
}
