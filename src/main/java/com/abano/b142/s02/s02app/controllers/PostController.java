package com.abano.b142.s02.s02app.controllers;

import com.abano.b142.s02.s02app.models.Post;
import com.abano.b142.s02.s02app.services.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
public class PostController {

    @Autowired
    PostService postService;

    @RequestMapping(value="/users/{userId}/posts/createPost", method=RequestMethod.POST)
    public ResponseEntity<Object> createPost(@RequestBody Post newPost, @PathVariable Long userId) {
        postService.createPost(newPost, userId);
        return new ResponseEntity<>("New Post Created!", HttpStatus.CREATED);
    }

    //retrieve all posts
    @RequestMapping(value="/posts/getPosts", method=RequestMethod.GET)
    public ResponseEntity<Object> getPosts() {
        return new ResponseEntity<>(postService.getPosts(), HttpStatus.OK);
    }

    //retrieve all posts from an existing user
    @RequestMapping(value="/users/{userId}/posts/getPosts", method=RequestMethod.GET)
    public ResponseEntity<Object> getMyPosts(@PathVariable Long userId) {
        return new ResponseEntity<>(postService.getMyPosts(userId), HttpStatus.OK);
    }

    @RequestMapping(value="/users/{userId}/posts/{postId}", method=RequestMethod.PUT)
    public ResponseEntity<Object> updatePost(@PathVariable Long userId,@PathVariable Long postId, @RequestBody Post updatedPost) {
        postService.updatePost(userId, postId, updatedPost);
        return new ResponseEntity<>("Post Updated!", HttpStatus.OK);
    }

    @RequestMapping(value="/posts/{id}/deletePost", method=RequestMethod.DELETE)
    public ResponseEntity<Object> deletePost(@PathVariable Long id) {
        postService.deletePost(id);
        return new ResponseEntity<>("Post Deleted!", HttpStatus.OK);
    }
}
